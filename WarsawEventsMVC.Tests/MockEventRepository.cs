﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

using WarsawEventsMVC.Models;
using WarsawEventsMVC.Models.Entities;

namespace WarsawEventsService.Tests
{
    class MockEventRepository : IEventRepository
    {
        List<Event> listEvent;

        public MockEventRepository()
        {
            listEvent = new List<Event>();
        }

        public void DeleteEvent(int eventId)
        {
            listEvent.RemoveAt(eventId);
        }

        public void Dispose()
        {
        }

        public Event Find(int id)
        {
            return listEvent[id];
        }

        public IQueryable<Event> GetCurrentEvents()
        {
            return listEvent.AsQueryable().Where((Event e) => e.DateOfStart > DateTime.Now);
        }

        public IQueryable<Event> GetEvents(IList<Expression<Func<Event, bool>>> funcList)
        {
            IQueryable<Event> query = listEvent.AsQueryable();

            foreach (Expression<Func<Event, bool>> func in funcList)
                query = query.Where(func);

            return query;
        }



        public void Save()
        {
        }

        public void UpdateEvent(Event updatedEvent)
        {

        }

        Event IEventRepository.InsertEvent(Event newEvent)
        {
            listEvent.Add(newEvent);
            return newEvent;
        }
    }
}

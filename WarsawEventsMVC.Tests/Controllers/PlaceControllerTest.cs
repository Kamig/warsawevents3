﻿using NUnit.Framework;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using WarsawEventsMVC.Controllers;
using WarsawEventsMVC.Models;
using WarsawEventsMVC.Models.Entities;
using WarsawEventsService.Tests;

namespace WarsawEventsMVC.Tests.Controllers
{
    [TestFixture]
    class PlaceControllerTest
    {

        [Test]
        public void IndexPlace()
        {
            //var rep = new MockRepository<Place>();
            ////rep.Insert(new Place());
            // Arrange
            PlacesController controller = new PlacesController(new MockRepository<Place>(), new MockRepository<Grade>());

            // Act
            ViewResult result = controller.Index(1) as ViewResult;
            var model = result.Model as ICollection<Place>;
            // Assert
            Assert.IsNotNull(result);
        }

        [Test]
        public void IndexPlaceModel()
        {
            var rep = new MockRepository<Place>();
            rep.Insert(new Place() { Id = 2 });
            // Arrange
            PlacesController controller = new PlacesController(rep, new MockRepository<Grade>());

            // Act
            ViewResult result = controller.Index(1) as ViewResult;
            var model = result.Model as IEnumerable<Place>;
            // Assert
            Assert.AreEqual(true, model.Any( p => p.Id == 2) );
        }


    }
}

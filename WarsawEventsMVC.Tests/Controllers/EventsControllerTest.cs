﻿using NUnit.Framework;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using WarsawEventsMVC.Controllers;
using WarsawEventsMVC.Models;
using WarsawEventsMVC.Models.Entities;
using WarsawEventsService.Tests;

namespace WarsawEventsMVC.Tests.Controllers
{
    [TestFixture]
    public class EventsControllerTest
    {

        [Test]
        public void IndexEvent()
        {

            IEventRepository rep = new MockEventRepository();
            var eventsController = new EventsController(rep, new MockRepository<Category>(), new MockRepository<District>(),
                new MockRepository<Place>(), new MockRepository<Participant>(), new MockRepository<Like>(), new MockRepository<Invitation>(), new MockRepository<Comment>());

            ViewResult result = eventsController.Index("", "Wszystkie Dzielince", "Wszystkie Kategorie", "Wszystkie",
                "Dacie", "", "", 1) as ViewResult;


            Assert.IsNotNull(result);
        }

        [Test]
        public void IndexEventModel()
        {
            Category c1 = new Category { CategoryName = "Test" };
            District d1 = new District { DistrictName = "Test" };
            Place p1 = new Place { District = d1, PlaceName = "Test", StreetName = "Test" };
            DateTime dt1 = DateTime.Now.AddYears(2);
   

            Event e1 = new Event { Id = 0, EventName = "Test1", Description = "Test", Place = p1, DateOfStart = dt1, Category = c1, Comments = new List<Comment>() };
          
            IEventRepository rep = new MockEventRepository();
            rep.InsertEvent(e1);
            var eventsController = new EventsController(rep, new MockRepository<Category>(), new MockRepository<District>(),
                new MockRepository<Place>(), new MockRepository<Participant>(), new MockRepository<Like>(), new MockRepository<Invitation>(), new MockRepository<Comment>());

            ViewResult result = eventsController.Index("", "Wszystkie Dzielince", "Wszystkie Kategorie", "Wszystkie",
                "Dacie", "", "", 1) as ViewResult;

            var model = result.Model as IEnumerable<EventViewModel>;
            // Assert
           Assert.AreEqual(true, model.Any(e => e.Id == 0));    
        }

        [Test]
        public void IndexEventSort()
        {
            Category c1 = new Category { CategoryName = "Test" };
            Category c2 = new Category { CategoryName = "Test" };
            District d1 = new District { DistrictName = "Test" };
            District d2 = new District { DistrictName = "Test" };
            Place p1 = new Place { District = d1, PlaceName = "Test", StreetName = "Test" };
            Place p2 = new Place { District = d2, PlaceName = "Test", StreetName = "Test" };
            DateTime dt1 = DateTime.Now.AddYears(2);
            DateTime dt2 = DateTime.Now.AddYears(1);

            Event e1 = new Event { Id = 0, EventName = "Test1", Description = "Test", Place = p1, DateOfStart = dt1, Category = c1, Comments = new List<Comment>() };
            Event e2 = new Event { Id = 1, EventName = "Test2", Description = "Test", Place = p2, DateOfStart = dt2, Category = c2, Comments = new List<Comment>() };

            IEventRepository rep = new MockEventRepository();
            rep.InsertEvent(e1);
            rep.InsertEvent(e2);

            var eventsController = new EventsController(rep, new MockRepository<Category>(), new MockRepository<District>(),
                new MockRepository<Place>(), new MockRepository<Participant>(), new MockRepository<Like>(), new MockRepository<Invitation>(), new MockRepository<Comment>());

            ViewResult result = eventsController.Index("", "Wszystkie Dzielince", "Wszystkie Kategorie", "Wszystkie",
                "Dacie", "", "", 1) as ViewResult;

            var model = result.Model as IEnumerable<EventViewModel>;
            // Assert
            Assert.AreEqual(true, model.FirstOrDefault().Id == e2.Id);
        }

        [Test]
        public void IndexEventFilter()
        {
            Category c1 = new Category { CategoryName = "Test" };
            Category c2 = new Category { CategoryName = "Test" };
            District d1 = new District { DistrictName = "Test" };
            District d2 = new District { DistrictName = "Test" };
            Place p1 = new Place { District = d1, PlaceName = "Test", StreetName = "Test" };
            Place p2 = new Place { District = d2, PlaceName = "Test", StreetName = "Test" };
            DateTime dt1 = DateTime.Now.AddYears(2);
            DateTime dt2 = DateTime.Now.AddYears(1);

            Event e1 = new Event { Id = 0, EventName = "Test1", Description = "Test", Place = p1, DateOfStart = dt1, Category = c1, Comments = new List<Comment>() };
            Event e2 = new Event { Id = 1, EventName = "Test2", Description = "Test", Place = p2, DateOfStart = dt2, Category = c2, Comments = new List<Comment>() };

            IEventRepository rep = new MockEventRepository();
            rep.InsertEvent(e1);
            rep.InsertEvent(e2);

            var eventsController = new EventsController(rep, new MockRepository<Category>(), new MockRepository<District>(),
                new MockRepository<Place>(), new MockRepository<Participant>(), new MockRepository<Like>(), new MockRepository<Invitation>(), new MockRepository<Comment>());

            ViewResult result = eventsController.Index("", "Wszystkie Dzielince", "Wszystkie Kategorie", "Wszystkie",
                "Dacie", "", "Test1", 1) as ViewResult;

            var model = result.Model as IEnumerable<EventViewModel>;
            // Assert
            Assert.AreEqual(true, model.Any(e => e.Id == e1.Id));
            Assert.AreEqual(false, model.Any(e => e.Id == e2.Id));
        }






    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarsawEventsMVC.Models;

namespace WarsawEventsService.Tests
{
    public class MockRepository<TEntity> : IRepository<TEntity>
          where TEntity : class
    {
        List<TEntity> tEntityList;

        public MockRepository()
        {
            tEntityList = new List<TEntity>();
        }

        public void Delete(TEntity concreteTEntity)
        {
            tEntityList.Remove(concreteTEntity);
        }

        public void Delete(int Id)
        {
            tEntityList.RemoveAt(Id);
        }

        public void Dispose()
        {

        }

        public TEntity Find(int id)
        {
            return tEntityList[id];
        }

        public IQueryable<TEntity> Get()
        {
            return tEntityList.AsQueryable();
        }

        public ICollection<TEntity> GetAll()
        {
            return tEntityList;
        }

        public void Insert(TEntity tEntity)
        {
            tEntityList.Add(tEntity);
        }

        public void Save()
        {
        }

        public void Update(TEntity tEntity)
        {
        }

        TEntity IRepository<TEntity>.Insert(TEntity tEntity)
        {
            throw new NotImplementedException();
        }
    }
}

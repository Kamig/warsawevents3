﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using WarsawEventsMVC.Models.Entities;

namespace WarsawEventsMVC.Models
{
    public static class FuncTool
    {
        public static Func<Event, object> GetSortFunc(string s)
        {
            s = s.ToLower();
            switch (s)
            {
                case "dacie":
                    return (Event e) => e.DateOfStart;
                case "cenie":
                    return (Event e) => e.Price;
                case "ilości uczestników":
                    return (Event e) => e.NumberOfParticipants;
                default:
                    return (Event e) => e.DateOfStart;
            }
        }

        public static Expression<Func<Event, bool>> GetPeriodFunc(string s)
        {
            s = s.ToLower();
            DateTime start,end;
            DateTime now = DateTime.Now;
            DateTime endOfWorld = now.AddYears(100);
            DateTime endOfWeekHelper = DateTime.Now.AddDays(7 - now.DayOfWeek.GetHashCode());

            switch (s)
            {
                case "wszystkie":
                    start = now;
                    end = endOfWorld;
                    break;
                case "dzisiejsze":
                    start = now;
                    end= new DateTime(now.Year, now.Month, now.Day, 23, 59, 59);
                    break;
                case "obecny tydzień":
                    start = now;
                    end= new DateTime(endOfWeekHelper.Year, endOfWeekHelper.Month, endOfWeekHelper.Day, 23, 59, 59);
                    break;
                case "następny tydzień":
                    DateTime endOfWeek = new DateTime(endOfWeekHelper.Year, endOfWeekHelper.Month, endOfWeekHelper.Day, 23, 59, 59);
                    DateTime nextWeekHelper = endOfWeek.AddDays(1); //DateTime to strucktura nie klasa
                    start = new DateTime(nextWeekHelper.Year, nextWeekHelper.Month, nextWeekHelper.Day, 0, 0, 0);
                    nextWeekHelper = nextWeekHelper.AddDays(6);
                    end = new DateTime(nextWeekHelper.Year, nextWeekHelper.Month, nextWeekHelper.Day, 23, 59, 59);
                    break;
                default:
                    start = now;
                    end = endOfWorld;
                    break;
            }

            return (Event e) => (e.DateOfStart >= start && e.DateOfStart <= end);
        }
        //"Wszystkie", "Utworzyłem", "Dołączyłem", "Polubiłem", "Polecane"
        public static Expression<Func<Event, bool>> GetTypeOfEventsFunc(string s, string userId, List<string> friends)
        {
            if(s != null)
            s = s.ToLower();

            if (string.IsNullOrEmpty(userId))
            {
                if (s == "wszystkie")
                    return (Event e) => true;
                else
                    return (Event e) => false;
            }

            switch (s)
            {
                case "wszystkie":
                    return (Event e) => true;

                case "utworzyłem":
                    return (Event e) => e.Participants.Any(
                        (Participant p) => p.ApplicationUserId == userId && p.IsOwner != null && p.IsOwner == true);

                case "dołączyłem":
                    return (Event e) => e.Participants.Any((Participant p) => p.ApplicationUserId == userId);

                case "polubiłem":
                    return (Event e) => e.Likes.Any((Like l) => l.ApplicationUserId == userId);

                case "polecane":

                    return (Event e) => e.Likes.Any((Like l) => friends.Contains(l.ApplicationUserId));

                default:
                    return (Event e) => true;
            }
        }


    }
}

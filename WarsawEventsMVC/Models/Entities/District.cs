﻿using System.Collections.Generic;

namespace WarsawEventsMVC.Models.Entities
{
    public class District: Entity
    {
        public string DistrictName { get; set; }

        public virtual ICollection<Place> Places { get; set; }
    }
}

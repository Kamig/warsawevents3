﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WarsawEventsMVC.Models.Entities
{
    public class Grade 
    {
        [Key, Column(Order = 0)]
        public int PlaceId { get; set; }
        [Key, Column(Order = 1)]
        public string ApplicationUserId { get; set; }
        [Range(1, 5)]
        public int Value { get; set; }

        public virtual Place Place { get; set; }//były zapetlenia
        //public virtual ApplicationUser ApplicationUser { get; set; }
    }
}

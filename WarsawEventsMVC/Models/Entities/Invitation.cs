﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarsawEventsMVC.Models.Entities
{
    public class Invitation
    {
        [Key, Column(Order = 0)]
        public string SendUserId { get; set; }

        [Key, Column(Order = 1)]
        public string ReceiveUserId { get; set; }
        public bool? IsAccepted { get; set; }
    }
}

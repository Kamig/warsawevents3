﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WarsawEventsMVC.Models.Entities
{
    public class Event: Entity
    {
        [Display (Name="Nazwa wydarzenia")]
        [Required( ErrorMessage="Podaj nazwe wydarzenia")]
        [StringLength(30)]
        public string EventName { get; set; }

        [Display(Name = "Opis wydarzenia")]
        [Required(ErrorMessage = "Podaj opis wydarzenia")]
        [StringLength(255)]
        public string Description { get; set; }

        [Display(Name = "Cena")]
        [Required(ErrorMessage = "Podaj cene")]
        public double Price { get; set; }

        [Display(Name = "Data rozpoczęcia")]
        [Required(ErrorMessage = "Podaj date")]
        public DateTime DateOfStart { get; set; }

        [Display(Name = "Liczba uczestników")]
        [Required(ErrorMessage = "Podaj liczbe uczestników")]
        public int NumberOfParticipants { get; set; }
        //Foreign keys

        
        public int PlaceId { get; set; }
        public int CategoryId { get; set; }
        public virtual Place Place { get; set; }
        public virtual Category Category { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Like> Likes { get; set; }
        public virtual ICollection<Participant> Participants { get; set; }

    }

    public class EventViewModel
    {
        public bool IsLike { get; set; }
        public bool IsParticipant { get; set; }
        public bool IsOwner { get; set; }

        [Key]
        public int Id { get; set; }

        [Display(Name = "Nazwa wydarzenia")]
        [Required(ErrorMessage = "Podaj nazwe wydarzenia")]
        [StringLength(30)]
        public string EventName { get; set; }

        [Display(Name = "Opis wydarzenia")]
        [Required(ErrorMessage = "Podaj opis wydarzenia")]
        [StringLength(255)]
        public string Description { get; set; }

        [Display(Name = "Cena")]
        [Required(ErrorMessage = "Podaj cene")]
        public double Price { get; set; }

        [Display(Name = "Data rozpoczęcia")]
        [Required(ErrorMessage = "Podaj date")]
        public DateTime DateOfStart { get; set; }

        [Display(Name = "Liczba uczestników")]
        [Required(ErrorMessage = "Podaj liczbe uczestników")]
        public int NumberOfParticipants { get; set; }
        //Foreign keys


        public int PlaceId { get; set; }
        public int CategoryId { get; set; }
        public virtual Place Place { get; set; }
        public virtual Category Category { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Like> Likes { get; set; }
        public virtual ICollection<Participant> Participants { get; set; }

        public static implicit operator EventViewModel(Event e)
        {
            return new EventViewModel()
            {
                Category = e.Category,
                CategoryId = e.CategoryId,
                Comments = e.Comments,
                DateOfStart = e.DateOfStart,
                Description = e.Description,
                EventName = e.EventName,
                Id = e.Id,
                IsLike = false,
                IsOwner = false,
                IsParticipant = false,
                Likes = e.Likes,
                NumberOfParticipants = e.NumberOfParticipants,
                Participants = e.Participants,
                Place = e.Place,
                PlaceId = e.PlaceId,
                Price = e.Price
            };

        }

        public static implicit operator Event(EventViewModel e)
        {
            return new Event()
            {
                Category = e.Category,
                CategoryId = e.CategoryId,
                Comments = e.Comments,
                DateOfStart = e.DateOfStart,
                Description = e.Description,
                EventName = e.EventName,
                Id = e.Id,
                Likes = e.Likes,
                NumberOfParticipants = e.NumberOfParticipants,
                Participants = e.Participants,
                Place = e.Place,
                PlaceId = e.PlaceId,
                Price = e.Price
            };

        }
    }
}

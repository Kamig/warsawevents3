﻿using Microsoft.AspNet.Identity;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WarsawEventsMVC.Models;
using WarsawEventsMVC.Models.Entities;

namespace WarsawEventsMVC.Controllers
{
    public class PlacesController : Controller
    {
        private WarsawEventsContext db = new WarsawEventsContext();

        IRepository<Place> repPlace;
        IRepository<Grade> repGrade;

        public PlacesController(IRepository<Place> repPlace, IRepository<Grade> repGrade)
        {
            
            this.repPlace = repPlace;
            this.repGrade = repGrade;
        }

        // GET: Places
        public ActionResult Index(int? page)
        {
            int pageSize = 6;
            int pageNumber = (page ?? 1);


            var places = repPlace.Get().Include(p => p.District).OrderBy(p=>p.PlaceName);
            //places.ForEach(p => p.Average = 0);
            //places.ForEach(p => repPlace.Update(p));
            //repPlace.Save();
            return View(places.ToPagedList(pageNumber,pageSize));
        }


        [HttpPost]
        [Authorize]
        public ActionResult UpdateAverage(int rate, int placeId, int? page)
        {
            int pageSize = 6;
            int pageNumber = (page ?? 1);
            string userId = User.Identity.GetUserId();
            Grade old = repGrade.Get().Where(g => g.PlaceId == placeId && g.ApplicationUserId == userId).FirstOrDefault();
            if (old != null) //uzytkownik ocenil juz to miejsce
            {
                ViewBag.Message = "Oceniłeś już to miejsce";
                return PartialView("_Places", repPlace.Get().Include(p => p.District).OrderBy(p => p.PlaceName).ToPagedList(pageNumber, pageSize));
            }
            repGrade.Insert(new Grade() { ApplicationUserId = userId, Value = rate, PlaceId = placeId });
            repGrade.Save();
            Place place = repPlace.Get().Where(p => p.Id == placeId).Include(p => p.Grades).FirstOrDefault();
            UpdatePlaceAverage(place);
            var places = repPlace.Get().Include(p => p.District).OrderBy(p => p.PlaceName).ToPagedList(pageNumber, pageSize);
            return PartialView("_Places", places);
        }

        private void UpdatePlaceAverage(Place place)
        {

            int sum = 0;
            foreach (var g in place.Grades)
            {
                sum += g.Value;
            }
            place.Average = (double)sum / place.Grades.Count;
            repPlace.Update(place);
            repPlace.Save();
        }

        // GET: Places/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Place place = db.Places.Find(id);
            if (place == null)
            {
                return HttpNotFound();
            }
            return View(place);
        }

        // GET: Places/Create
        public ActionResult Create()
        {
            ViewBag.DistrictId = new SelectList(db.Districts, "Id", "DistrictName");
            return View();
        }

        // POST: Places/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,PlaceName,StreetName,Streetnumber,Average,DistrictId")] Place place)
        {
            if (ModelState.IsValid)
            {
                db.Places.Add(place);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.DistrictId = new SelectList(db.Districts, "Id", "DistrictName", place.DistrictId);
            return View(place);
        }

        // GET: Places/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Place place = db.Places.Find(id);
            if (place == null)
            {
                return HttpNotFound();
            }
            ViewBag.DistrictId = new SelectList(db.Districts, "Id", "DistrictName", place.DistrictId);
            return View(place);
        }

        // POST: Places/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,PlaceName,StreetName,Streetnumber,Average,DistrictId")] Place place)
        {
            if (ModelState.IsValid)
            {
                db.Entry(place).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DistrictId = new SelectList(db.Districts, "Id", "DistrictName", place.DistrictId);
            return View(place);
        }

        // GET: Places/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Place place = db.Places.Find(id);
            if (place == null)
            {
                return HttpNotFound();
            }
            return View(place);
        }

        // POST: Places/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Place place = db.Places.Find(id);
            db.Places.Remove(place);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

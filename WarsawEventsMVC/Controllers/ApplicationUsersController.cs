﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WarsawEventsMVC.Models;
using WarsawEventsMVC.Models.Entities;

namespace WarsawEventsMVC.Controllers
{
    public class ApplicationUsersController : Controller
    {
        private WarsawEventsContext db = new WarsawEventsContext();

        // GET: ApplicationUsers
        IRepository<ApplicationUser> repUser;
        IRepository<Invitation> repInvitation;
        public ApplicationUsersController(IRepository<ApplicationUser> repUser, IRepository<Invitation> repInvitation)
        {
            this.repUser = repUser;
            this.repInvitation = repInvitation;
        }

        public ActionResult Index(string filter)
        {
           
            ViewBag.TypeOfUsers = filter;
            string id = "";
            if (User != null && User.Identity.GetUserId() != null)
            {
                id = User.Identity.GetUserId();
            }

            return View(repUser.GetAll().Where(u => u.Id != id));
        }

        public ActionResult UpdateUsers(string filter)
        {
            ViewBag.TypeOfUsers = filter;
            List<string> users = null;
            string id = "";
            if (User.Identity.GetUserId() != null)
            {
                 id = User.Identity.GetUserId();
                ApplicationUser user = repUser.Get().Where(u => u.Id == id)
                    .Include(u => u.SendInvitations).Include(u => u.ReceiveInvitations).FirstOrDefault();
                users = FilterUsersTool.GetUserList(filter, user);
            }

           

            IEnumerable<ApplicationUser> entity;

            if (users == null)
            {
                entity = repUser.GetAll().Where(u => u.Id != id) ;
            }
            else
            {
                entity = repUser.Get().Where(u => users.Contains(u.Id) && u.Id != id).ToList();
            }
            return PartialView("_ApplicationUsers", entity);
        }

        // GET: ApplicationUsers/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = repUser.Find(Int32.Parse(id));
            if (applicationUser == null)
            {
                return HttpNotFound();
            }
            return View(applicationUser);
        }

        // GET: ApplicationUsers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ApplicationUsers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEndDateUtc,LockoutEnabled,AccessFailedCount,UserName")] ApplicationUser applicationUser)
        {
            if (ModelState.IsValid)
            {
                repUser.Insert(applicationUser);
                repUser.Save();
                return RedirectToAction("Index");
            }

            return View(applicationUser);
        }

        public ActionResult Accept(string id)
        {
            string RecId = User.Identity.GetUserId();
            Invitation inv = repInvitation.Get().Where(i => i.SendUserId == id && i.ReceiveUserId == RecId).FirstOrDefault();
            if(inv != null)
            {
                ViewBag.Message = "Zaakceptowano zaproszenie.";
                inv.IsAccepted = true;
                repInvitation.Update(inv);
                repInvitation.Save();
            }
            string Id = "";
            if (User.Identity.GetUserId() != null)
            {
                Id = User.Identity.GetUserId();
            }

            return View("~/Views/ApplicationUsers/Index.cshtml", repUser.GetAll().Where(u => u.Id != Id)) ;
        }
        public ActionResult Invite(string id)
        {
            string Id = "";
            if (User.Identity.GetUserId() != null)
            {
                Id = User.Identity.GetUserId();
            }

            string SendId = User.Identity.GetUserId();
            Invitation old = repInvitation.Get().Where(i => i.SendUserId == SendId && i.ReceiveUserId == id).FirstOrDefault();
            if (old != null)
            {
                ViewBag.Message = "Już wcześniej wysłano zaproszenie.";
                return View("~/Views/ApplicationUsers/Index.cshtml", repUser.GetAll().Where(u => u.Id != Id) );
            }//throw new HttpResponseException(HttpStatusCode.Forbidden);////return Request.CreateResponse(HttpStatusCode.Forbidden);
            Invitation recInv = repInvitation.Get().Where(i => i.SendUserId == id && i.ReceiveUserId == SendId).FirstOrDefault();
            if(recInv != null)
            {
                if (recInv.IsAccepted == true)
                {
                    ViewBag.Message = "Jesteście już znajomymi.";
                }
                else
                {
                    recInv.IsAccepted = true;
                    repInvitation.Update(recInv);
                    repInvitation.Save();
                    ViewBag.Message = "Użytkownik, którego chcesz zaprosić wysłał Ci już zaproszenie, więc zostaliście znajomymi";
                }
                return View("~/Views/ApplicationUsers/Index.cshtml", repUser.GetAll().Where(u => u.Id != Id));
            }
            repInvitation.Insert(new Invitation() { SendUserId = SendId, ReceiveUserId = id, IsAccepted = false });
            repInvitation.Save();

            ViewBag.Message = "Wysłano zaproszenie.";

            return View("~/Views/ApplicationUsers/Index.cshtml",repUser.GetAll().Where(u => u.Id != Id));

        }

        // GET: ApplicationUsers/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = repUser.Find(Int32.Parse(id));
            if (applicationUser == null)
            {
                return HttpNotFound();
            }
            return View(applicationUser);
        }

        // POST: ApplicationUsers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEndDateUtc,LockoutEnabled,AccessFailedCount,UserName")] ApplicationUser applicationUser)
        {
            if (ModelState.IsValid)
            {
                db.Entry(applicationUser).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(applicationUser);
        }

        // GET: ApplicationUsers/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = repUser.Find(Int32.Parse(id));
            if (applicationUser == null)
            {
                return HttpNotFound();
            }
            return View(applicationUser);
        }

        // POST: ApplicationUsers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            ApplicationUser applicationUser = repUser.Find(Int32.Parse(id));
            repUser.Delete(Int32.Parse(applicationUser.Id));
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
